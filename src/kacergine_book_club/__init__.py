"""Global configurations and imports for Kacergine Book Club project."""

from pathlib import Path

__version__ = '1.0.0'
__schedule__ = Path('db/schedule.yaml')

from .book import Book
from .builder import build

__all__ = [Book, build]
