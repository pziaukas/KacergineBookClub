"""Implement Book entity."""

from datetime import date
import requests
from bs4 import BeautifulSoup

BASE_URL = 'https://www.goodreads.com/book/show/'


class RequestBookException(Exception):
    pass


class Book:
    """Book entity that holds relevant information."""
    def __init__(self, uuid: str, due_date: date) -> None:
        """Initialize the entity with its UUID and due date."""
        self.url = f'{BASE_URL}{uuid}'
        self.due_date = due_date
        self.fetch_metadata()

    def fetch_metadata(self) -> None:
        """Fetch additional metadata."""
        attribute_values = {}
        try:
            print(f'Requesting {self.url} ...')
            page = requests.get(self.url)
            print(f'... status {page.status_code} - {page.reason}.')
            if page.status_code != 200:
                raise RequestBookException

            soup = BeautifulSoup(page.content, 'html.parser')
            print('Parsing attribute values ...')
            attribute_values['title'] = soup.find('h1', {'id': 'bookTitle'}).text.strip()
            attribute_values['authors'] = ' '.join(soup.find('div', {'id': 'bookAuthors'}).text.split()[1:])
            attribute_values['cover_url'] = soup.find('img', {'id': 'coverImage'})['src']
            print('... looks OK.')
        except (RequestBookException, AttributeError, TypeError) as error:
            print(error)
            exit('Could not get data from GoodReads!')
        else:
            for attribute, value in attribute_values.items():
                setattr(self, attribute, value)
        finally:
            print(attribute_values)
