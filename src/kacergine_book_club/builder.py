from kacergine_book_club import __schedule__, Book
import yaml
from jinja2 import Environment, FileSystemLoader
from datetime import datetime


def get_latest_book() -> Book:
    """Obtain the latest (next) book."""
    with open(__schedule__, 'r') as file:
        uuid_by_date = yaml.safe_load(file)
    date, uuid = max(uuid_by_date.items())
    return Book(uuid, date)


def build() -> None:
    """Build the static website based on the relevant book information."""
    book = get_latest_book()

    environment = Environment(loader=FileSystemLoader('public'), trim_blocks=True)
    template = environment.get_template('index.html.jj')
    content = template.render(book=book.__dict__, year=datetime.now().year)

    with open('public/index.html', 'w') as file:
        file.write(content)
    print('All done!')


if __name__ == '__main__':
    build()
