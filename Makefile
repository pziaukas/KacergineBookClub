.PHONY: environment build test
$(VERBOSE).SILENT:

#################################################################################
# GLOBALS                                                                       #
#################################################################################

EXECUTOR = poetry run

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install (or update) Python environment
environment:
	poetry install

## Build the leader-board HTML
build:
	$(EXECUTOR) build

## Run the test suite using pytest
test:
	$(EXECUTOR) pytest
